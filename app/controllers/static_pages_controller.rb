class StaticPagesController < ApplicationController
  def home
    if logged_in?
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
      
    #    #条件分岐
    #@micropost = if params[:search]
    #  #searchされた場合は、原文+.where('name LIKE ?', "%#{params[:search]}%")を実行
    #  Micropost.where(activated: true).paginate(page: params[:page]).where('name LIKE ?', "%#{params[:search]}%")
    #else
    # #searchされていない場合は、原文そのまま
    #  Micropost.where(activated: true).paginate(page: params[:page])
    #end
    end

    # app/views/リソース名/アクション名.html.erb
    # app/views/static_pages/home.html.erb
  end

  def help
  end

  def about
    # 'app/views/static_pages/about.html.erb'
  end

  def contact
    # app/views/static_pages/contact.html.erb'
  end
end
